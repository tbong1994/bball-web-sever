import { Component, OnInit, Inject, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AppService } from '../app.component/app.component.service';
import { Player } from './../player.component/player';
import { DataConstants } from './../DataConstants';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from './../dialog.component/dialog.component';
import { DialogService } from './../dialog.component/dialog.service';
import { LoginService } from './login.service';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { of } from 'rxjs/observable/of';
@Component({
  selector: 'bball-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', './../css/dialog.css', './../css/font.css', './../css/animation.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  private dialogRef: MatDialogRef<DialogComponent, any>;
  public player: Player;
  public message: string;
  public readonly playerTitle: string = 'Player';
  public readonly dialogTitle: string = 'User Login';
  private userToken: string;

  @ViewChild('messageSpan') messageSpan: ElementRef;
  @ViewChild('dialogTitleDiv') dialogTitleDiv: ElementRef;
  @Output() draggableHandleEmitter = new EventEmitter<ElementRef>();

  constructor(
    private dialogService: DialogService,
    private loginService: LoginService,
    private router: Router,
    private appService: AppService
  ) { }
  ngAfterViewInit() {

  }

  ngOnInit() {
    this.dialogRef = this.dialogService.getDialogRef();
    this.player = this.dialogService.getPlayer();
    this.message = this.dialogService.getDialogMessage();

    this.userSessionForThisUserExists()
      .flatMap((sessionExists) => {
        if (sessionExists) {
          return this.isUserSessionValid();
        }
        return of(false);
      })
      .catch((err) => {
        this.appService.navigateToErrorPage(err, this.constructor.name);
        return of(false);
      })
      .subscribe((userSessionValid) => {
        if (userSessionValid) {
          this.updateCurrentUser();
          this.navigateToNavigationDialog();
          return;
        }
      });
  }

  userSessionForThisUserExists(): Observable<boolean> {
    const localStorage = this.loginService.getLocalStorage();
    const userToken = localStorage.getItem(DataConstants.localStorageDataConstants.userTokenKey);
    const previousUserID = localStorage.getItem(DataConstants.localStorageDataConstants.userIDKey);

    const radix = 10;
    return of(userToken && previousUserID && (this.player.ID === parseInt(previousUserID, radix)));
  }

  isUserSessionValid(): Observable<boolean> { // user session should expire if inactive for 30 minutes
    const localStorage = this.loginService.getLocalStorage();
    const [hour, minute] = localStorage.getItem(DataConstants.localStorageDataConstants.userTokenExpirationKey).split('%');

    const now = new Date();
    const currentHour = now.getHours();
    const currentMinute = now.getMinutes();

    const radix = 10;
    const hourDifference = currentHour - parseInt(hour, radix);
    if (hourDifference < 0 || hourDifference >= 2) { // last user active time was at a later time than now today or at least 1 hour+ has passed since last active time
      return of(false);
    }
    let minuteDifference = currentMinute - parseInt(minute, radix);
    if (minuteDifference < 0) {
      minuteDifference += 60; // add 60 when the difference is negative because 1 hour is 60 minutes
    }
    return of(minuteDifference < 30); // minuteDifference is greater than 30 mins or not
  }

  onLogInClicked(pin: HTMLInputElement): void {
    this.validatePin(pin.value)
      .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
      .flatMap((res: any) => {
        if (res.authenticated) {
          this.userToken = res.token || 'n0T0k3nG3N3r83d';
          this.authenticateUser();
          return of(true);
        }
        return of(false);
      })
      .subscribe(isValidated => {
        if (!isValidated) {
          this.loginNotValidated(pin);
        }
      });
  }

  loginNotValidated(pin: HTMLInputElement): Observable<boolean> {
    this.message = 'invalid PIN';
    this.messageSpan.nativeElement.classList.remove('pinMessageSpan');
    this.messageSpan.nativeElement.classList.add('invalidPinMessage');
    pin.value = '';
    return of(false);
  }

  navigateToNavigationDialog(): void {
    this.dialogService.navigateToNavigationComponent();
  }

  validatePin(pin: string): Observable<any> {
    if (pin.length !== 4) {
      /**
       * TODO: create a data constant npm module and use that to serialize data instead of hard coding it
       */
      return of(JSON.stringify({ 'authenticated': false }));
    }
    return this.loginService.validatePin(this.player, pin);
  }

  onCancelClicked(): void {
    this.dialogRef.close();
  }

  authenticateUser(): void {
    this.updateUserSession();
    return this.navigateToNavigationDialog();
  }

  updateUserSession(): void {
    const localStorage = this.loginService.getLocalStorage();
    localStorage.setItem(DataConstants.localStorageDataConstants.userTokenKey, this.userToken);

    const now = new Date();
    const lastUserActiveTime = now.getHours() + '%' + now.getMinutes();
    localStorage.setItem(DataConstants.localStorageDataConstants.userTokenExpirationKey, lastUserActiveTime);
    localStorage.setItem(DataConstants.localStorageDataConstants.userIDKey, this.player.ID.toString());
    this.updateCurrentUser();
  }

  updateCurrentUser(): void {
    this.loginService.updateUserToken(this.userToken);
    this.loginService.updateCurrentUser(this.player);
  }
}
