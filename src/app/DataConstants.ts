/**
 * @class all constant variables for the application
 */
export class DataConstants {
    public static readonly localStorageDataConstants = {
        userTokenKey: 'userSessionToken',
        userTokenExpirationKey: 'userSessionLastActiveTime',
        userIDKey: 'currentUser'
    };

    public static readonly dialogNames = {
        login: 'login',
        editProfile: 'edit-profile',
        deleteAccount: 'delete-account',
        changedSaved: 'changes-saved',
        navigation: 'navigation',
        remove: 'remove',
        signUp: 'sign-up',
        updateScore: 'update-score',
        vote: 'vote',
        sessionTimeoutPage: 'session-timeout',
        changePinPage: 'change-pin'
    };

    public static readonly dialogDataConstants = {
        dialogName: 'dialogName',
        player: 'player',
        dialogRef: 'dialogRef',
        displayMessage: 'message',
        userPin: 'user-pin'
    };

    public static readonly pageNameConstantData = {
        homePage: 'home',
        signupPage: 'signup',
        errorPage: 'error'
    };

    public static readonly hourConstantData = {
        hour: 'hour',
        mins: 'minutes'
    };

    public static readonly errorDataConstants = {
        errorMessage: 'errorMessage',
        errorComponentName: 'errorComponentName'
    };
    public static readonly cacheKeys = {
        playerListKey: 'players',
        playerScoreKey: 'playerScore',
        lastGameScoreKey: 'lastGameScore'
    };
}
