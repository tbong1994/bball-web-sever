import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//SERVICES
import { AppService } from './app.component/app.component.service';

//COMPONENTS
import { SignUpComponent } from './signup.component/signup.component';
import { WelcomeComponent } from './welcome.component/welcome.component';
import { EditProfileComponent } from './edit-profile.component/edit-profile.component';
import { VoteComponent } from './vote.component/vote.component';
import { DialogComponent } from './dialog.component/dialog.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ChangesSavedComponent } from './changes-saved.component/changes-saved.component';
import { SignupDialogComponent } from './signup-dialog/signup-dialog.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { RemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { UpdateScoreComponent } from './update-score/update-score.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ChangePinComponent } from './change-pin/change-pin.component';

//DATA CONSTANTS
import { DataConstants } from './DataConstants';

const appRoute: Routes = [
    { path: '', redirectTo: '/' + DataConstants.pageNameConstantData.homePage, pathMatch: 'full' },
    { path: '*', redirectTo: '/ ' + DataConstants.pageNameConstantData.homePage, pathMatch: 'full' },
    { path: DataConstants.pageNameConstantData.homePage, component: WelcomeComponent },
    { path: DataConstants.pageNameConstantData.signupPage, component: SignUpComponent },
    { path: DataConstants.dialogNames.editProfile, component: EditProfileComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.login, component: LoginComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.changedSaved, component: ChangesSavedComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.navigation, component: NavigationComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.signUp, component: SignupDialogComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.remove, component: RemoveDialogComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.deleteAccount, component: DeleteAccountComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.updateScore, component: UpdateScoreComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.dialogNames.changePinPage, component: ChangePinComponent, outlet: 'dialogOutlet' },
    { path: DataConstants.pageNameConstantData.errorPage, component: ErrorPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoute/*,{enableTracing:true}*/)], // enableTracing is for debugging
    exports: [RouterModule]
})
export class AppRouterModule {
}
