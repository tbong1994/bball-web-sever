import { Component, OnInit } from '@angular/core';
import { DeleteAccountService } from './delete-account.service';
import { DialogService } from './../dialog.component/dialog.service';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';
import { Player } from './../player.component/player';
import { DialogComponent } from './../dialog.component/dialog.component';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from '../DataConstants';
@Component({
  selector: 'bball-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.css', './../css/dialog.css', './../css/font.css', './../css/animation.css']
})

export class DeleteAccountComponent implements OnInit {

  public readonly dialogTitle: string = 'Delete Account';
  public readonly message: string = 'Are you sure you\'d like to remove your account?';
  public readonly playerTitle: string = 'Player';
  public player: Player;
  private dialogRef: MatDialogRef<DialogComponent, any>;
  constructor(
    private deleteAccountService: DeleteAccountService,
    private dialogService: DialogService,
    private appService: AppService,
    private router: Router
  ) { }

  ngOnInit() {
    this.player = this.dialogService.getPlayer();
    this.dialogRef = this.dialogService.getDialogRef();
  }

  onDeleteClicked(): void {
    this.deleteAccountService.delete(this.player)
      .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
      .subscribe((res) => {
        alert('You\'re welcome to come back anytime!');

        const dialogData = new Map<string, any>();
        dialogData.set(DataConstants.dialogDataConstants.player, this.player);
        dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.deleteAccount);
        this.dialogRef.close(dialogData);
      });
  }

  onCancelClicked(): void {
    this.goToNavigationComponent();
  }

  goToNavigationComponent(): void {
    this.dialogService.navigateToNavigationComponent();
  }

}
