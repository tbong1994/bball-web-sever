import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './app.component.service';
import { DataConstants } from './../DataConstants';
import { SessionService } from './../session/session.service';
@Component({
    selector: 'bball-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css', './../css/font.css', './../css/animation.css']
})
export class AppComponent implements OnInit {

    @ViewChild('body') bodyElementRef: ElementRef;
    public readonly header: string = 'playing basketball today?';
    public readonly title: string = 'Hyland Basketball Attendance';
    public readonly today: number = Date.now();
    private readonly timeoutMessage = 'Your session is about to expire. Would you like to stay signed in?';
    private timeoutID; //5 min timeout

    constructor(
        private router: Router,
        private appService: AppService,
        private sessionService: SessionService
    ) { }

    ngOnInit() {
        this.appService.setBodyDOMElement(this.bodyElementRef.nativeElement.parentElement.parentElement);
    }
}
