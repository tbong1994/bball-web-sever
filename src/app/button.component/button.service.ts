

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class ButtonService {

    private headers: HttpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });
    private yesOrNo: string;

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }
}
