
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { AppService } from '../app.component/app.component.service';
import { Player } from '../player.component/player';
import { DataConstants } from './../DataConstants';
@Injectable()
export class PlayerService {

    private url: string = this.appService.url;
    private headers: HttpHeaders = this.appService.headers;
    private selectedPlayer: Player;
    public clickedPlayer: Player; //player whose pin has not been validated yet
    public readonly playerListCacheKey = DataConstants.cacheKeys.playerListKey;

    constructor(
        private http: HttpClient,
        private appService: AppService
    ) { }

    getPlayers(): Observable<Player[]> {
        const url = `${ this.url }/players`;
        return this.http.get<Player[]>(url).flatMap(this.deserializePlayers);
    }
    /**
     * @function deserializePlayers deserializes and converts the array of SQL objects into Player objects.
     * @param playerObjects array of sql objects
     */
    deserializePlayers(playerObjects: Player[]): Observable<Player[]> {
        const players: Player[] = [];
        for (let i = 0; i < playerObjects.length; i++) {
            const player: Player = new Player(playerObjects[i].firstname, playerObjects[i].lastname, playerObjects[i].email, playerObjects[i].imgurl);
            player.setID(playerObjects[i].ID);
            player.setAdmin(playerObjects[i].isAdmin);
            player.setAnswer(playerObjects[i].answer);
            player.setAnswerDate(playerObjects[i].answerdate);
            player.setHeight(playerObjects[i].height);
            player.setSpeed(playerObjects[i].speed);
            player.setRating(playerObjects[i].rating);
            player.setStats(playerObjects[i].stats);
            players.push(player);
        }
        return of(players);
    }
    updatePlayers(players: Player[]): void {
        this.appService.updatePlayers(players);
    }

    updateSelectedPlayer(selectedPlayer: Player) {
        this.selectedPlayer = selectedPlayer;
    }

    getSelectedPlayer(): Player {
        return this.selectedPlayer;
    }

    getPlayerByName(firstname: string, lastname: string): Observable<Player> {
        const url = `${ this.url }/players/${ firstname }/${ lastname }`;
        return this.http.get<Player>(url, { headers: this.headers });
    }

    getTodaysDate(): string {
        return this.appService.getTodaysDate();
    }

    updateTeams(team1: Player[], team2: Player[]): Observable<void> {
        const url = `${ this.url }/players/update-teams`;
        const playersIDAndTeam = this.serializePlayerAndTeam(team1, team2);

        return this.http.put<void>(url, JSON.stringify(playersIDAndTeam), { headers: this.headers });
    }

    serializePlayerAndTeam(team1: Player[], team2: Player[]): Player[] {
        const playersIDAndTeam: Player[] = [];
        const bothTeams = [...team1, ...team2]; // concat 2 arrays into 1
        for (let i = 0; i < bothTeams.length; i++) {
            playersIDAndTeam.push(bothTeams[i]);
        }
        return playersIDAndTeam;
    }

    updateSeventhPlayer(): Observable<void> {
        const url = `${ this.url }/players/set/alternate`;
        return this.http.put<void>(url, { headers: this.headers });
    }

    getSeventhPlayer(): Observable<any> {
        const url = `${ this.url }/players/get/alternate`;
        return this.http.get<any>(url, { headers: this.headers });
    }

    getScoreUpdatedTime(): Observable<string> {
        const url = `${ this.url }/score/last-update-date`;
        return this.http.get<string>(url, { headers: this.headers });
    }
}
