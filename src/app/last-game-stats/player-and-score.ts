import { Player } from './../player.component/player';
import { AppService } from './../app.component/app.component.service';
export class PlayerAndScoreElement {

    constructor(
        protected firstname: string,
        protected team: number,
        protected score: number,
        protected date: string) {
    }


    getFirstname(): string {
        return this.firstname;
    }

    getTeam(): number {
        return this.team;
    }

    getScore(): number {
        return this.score;
    }

    getDate(): string {
        return this.date;
    }
}
