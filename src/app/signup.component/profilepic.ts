export class ProfilePic {

    imgurl: string;
    clicked: boolean;

    constructor(imgurl: string, clicked: boolean) {
        this.imgurl = imgurl;
        this.clicked = clicked;
    }

    setImgUrl(imgurl: string): void {
        this.imgurl = imgurl;
    }
    setImgClicked(clicked: boolean): void {
        this.clicked = clicked;
    }
    getImgUrl(): string {
        return this.imgurl;
    }
    getImgClicked(): boolean {
        return this.clicked;
    }
}
