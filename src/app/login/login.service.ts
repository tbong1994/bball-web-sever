import { Injectable } from '@angular/core';
import { Player } from './../player.component/player';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from './../app.component/app.component.service';
import { SessionService } from './../session/session.service';
@Injectable()
export class LoginService {

  private url: string = this.appService.url;
  private headers: HttpHeaders = this.appService.headers;
  private player: Player;

  constructor(
    private appService: AppService,
    private http: HttpClient,
    private sessionService: SessionService
  ) { }

  validatePin(player: Player, pin: string): Observable<any> {
    const params = { 'id': `${ player.ID }`, 'pin': pin };
    const url = `${ this.url }/players/pin`;
    return this.http.get<any>(url, { headers: this.headers, params: params });
  }

  /**
   * @function updateLoggedInPlayer() updates the player instance variable to this player who just logged in.
   * @param player player whose login has been validated.
   */
  updateCurrentUser(player: Player): void {
    this.sessionService.updateCurrentUser(player);
  }
  /**
   * @function getLoggedInPlayer() returns the currently logged in player
   */
  getCurrentUser(): Player {
    return this.sessionService.getCurrentUser();
  }
  getLocalStorage(): Storage {
    return this.sessionService.getLocalStorage();
  }
  updateUserToken(token: string): void {
    this.sessionService.updateUserToken(token);
  }
  getUserToken(): string {
    return this.sessionService.getUserToken();
  }
}
