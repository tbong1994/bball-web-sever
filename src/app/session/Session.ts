
export class Session {

    private session: Map<string, string>;
    private session_id: string;
    private session_token: string;
    public size: number;
    constructor(sessionId: string, sessionToken: string) {
        this.session_id = sessionId;
        this.session_token = sessionToken;
        this.session.set(this.session_id, this.session_token);
        this.size = this.session.size;
    }

    getSessionId(): string {
        return this.session_id;
    }

    getSessionToken(): string {
        return this.session_token;
    }
    get(userSessionId: string): string {
        if (this.session.get(userSessionId) !== undefined) {
            return this.session.get(userSessionId);
        }
        return undefined;
    }
    set(userSessionId: string, userSessionToken: string) {
        this.session.set(userSessionId, userSessionToken);
    }
}
