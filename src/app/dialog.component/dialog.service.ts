import { Injectable } from '@angular/core';
import { AppService } from './../app.component/app.component.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Player } from '../player.component/player';
import { DialogComponent } from './dialog.component';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { DataConstants } from '../DataConstants';
import { Component } from '@angular/compiler/src/core';
@Injectable()
export class DialogService {

    private player: Player;
    private dialogRef: MatDialogRef<DialogComponent, any>;
    private dialogName: string;
    private dialogData: any;
    private dialogMessage: string;
    private timeoutID;
    private draggableHandle: HTMLElement;
    constructor(
        private http: HttpClient,
        private appService: AppService,
        private router: Router
    ) { }

    updateDialogType(dialogName: string): void {
        this.dialogName = dialogName;
    }
    /**
     * @function updatePlayer() updates the player instance variable to this player whose dialog is open.
     * @param player player whose dialog just opened
     */
    updatePlayer(player: Player): void {
        this.player = player;
    }
    updateDialogRef(dialogRef: MatDialogRef<DialogComponent, any>): void {
        this.dialogRef = dialogRef;
    }
    updateDialogData(data: any) {
        this.dialogData = data;
    }
    updateDialogMessage(msg: string) {
        this.dialogMessage = msg;
    }
    /**
     * @function getPlayer() returns the player whose dialog is open
     */
    getPlayer(): Player {
        if (this.player !== undefined) {
            return this.player;
        }
    }
    getDialogType(): string {
        return this.dialogName;
    }
    getDialogRef(): MatDialogRef<DialogComponent, any> {
        return this.dialogRef;
    }
    getDialogData(): any {
        return this.dialogData;
    }
    getDialogMessage(): string {
        return this.dialogMessage;
    }
    getApplicationBodyDOMElement(): HTMLBodyElement {
        return this.appService.getBodyDOMElement();
    }
    navigateToNavigationComponent(): void {
        this.appService.navigateToComponent(DataConstants.dialogNames.navigation, true);
    }
    /**
     * @param pageName name of the page to navigate to.
     * @param isDialog whether the page should be navigated within the dialog or not.
     */
    navigateToComponent(pageName: string, isDialog: boolean) {
        this.appService.navigateToComponent(pageName, isDialog);
    }
    navigateToErrorPage(errorMessage: string, errorComponent: any): void {
        this.appService.navigateToErrorPage(errorMessage, errorComponent);
    }
}
