import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppService } from './../app.component/app.component.service';
@Injectable()
export class ChangePinService {

  private url: string = this.appService.url;
  private headers: HttpHeaders = this.appService.headers;
  constructor(
    private appService: AppService,
    private router: Router,
    private http: HttpClient
  ) { }

  updatePin(pin: string, playerID: number): Observable<any> {
    const url = `${ this.url }/players/update-pin`;
    const userPinData = { 'id': `${ playerID }`, 'pin': `${ pin }` };
    const userDataJSON = JSON.stringify(userPinData);
    return this.http.put<any>(url, userDataJSON, { headers: this.headers });
  }
}
