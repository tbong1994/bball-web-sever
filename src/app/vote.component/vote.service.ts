
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Player } from '../player.component/player';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from 'virtualRoot/DataConstants';

@Injectable()
export class VoteService {

    private readonly url: string = this.appService.url;
    private headers: HttpHeaders = this.appService.headers;
    private vote: string;


    constructor(
        private http: HttpClient,
        private appService: AppService
    ) { }

    save(player: Player): Observable<void> {
        const url = `${ this.url }/players/vote/save`;

        // only id is needed as DB Query gets the todaysdate for us, and the vote value must be 1 if this http is reached.
        const userVoteDataInJson = JSON.stringify([player.ID]);
        // this.modifyPlayerFromCacheProperty(player.ID);
        return this.http.put<void>(url, userVoteDataInJson, { headers: this.headers });
    }

    // modifyPlayerFromCacheProperty(ID: number): void {
    //     const playerListCacheKey = DataConstants.cacheKeys.playerListKey;
    //     const players: Player[] = AppService.appCache.get(playerListCacheKey);
    //     players.find(p => p.ID === ID).setAnswer(1);
    //     debugger;
    // }

    getCurrentTime(): string {
        return this.appService.getCurrentTime();
    }
}
