import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Player } from './../player.component/player';
import { LoginService } from './../login/login.service';
import { AppService } from '../app.component/app.component.service';
import { DataConstants } from '../DataConstants';
import { NavigationService } from './navigation.service';
@Component({
  selector: 'bball-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css', './../css/dialog.css', './../css/font.css', './../css/animation.css']
})
/*This Component is for the user to navigate to other components(Vote, EditProfile) after login */
export class NavigationComponent implements OnInit {

  public player: Player;
  public readonly dialogTitle: string = 'HBA';
  public readonly playerTitle: string = 'player';
  public isAdmin: boolean;
  public readonly dialogName: string = 'dialogName';
  public readonly buttonDisplayString: string = 'buttonDisplayString';
  public scoreSpan;
  public buttons: Map<string, string>[];

  constructor(
    private router: Router,
    private loginService: LoginService,
    private appService: AppService,
    private navService: NavigationService
  ) { }

  ngOnInit() {
    this.player = this.loginService.getCurrentUser();
    this.isAdmin = this.player.isAdmin;
    this.buttons = this.getButtons();
    this.getPlayerScore();
  }
  /**
   * @function initializeButtonValues initializes the button value and display string for each nav button
   */

  getButtons(): Map<string, string>[] {
    const buttons: Map<string, string>[] = [];
    const editProfileButtonMap = new Map<string, string>();
    const deleteButtonMap = new Map<string, string>();
    const updateScoreButtonMap = new Map<string, string>();

    editProfileButtonMap.set('dialogName', DataConstants.dialogNames.editProfile);
    editProfileButtonMap.set('buttonDisplayString', 'Edit Profile');

    deleteButtonMap.set('dialogName', DataConstants.dialogNames.deleteAccount);
    deleteButtonMap.set('buttonDisplayString', 'Delete Account');

    buttons.push(editProfileButtonMap);
    buttons.push(deleteButtonMap);

    if (this.isAdmin) {
      updateScoreButtonMap.set('dialogName', DataConstants.dialogNames.updateScore);
      updateScoreButtonMap.set('buttonDisplayString', 'Update Score');
      buttons.push(updateScoreButtonMap);
    }
    return buttons;
  }

  onNavBarClicked(value: string): void {
    this.appService.navigateToComponent(value, true);
  }

  getPlayerScore(): void {
    this.navService.getPlayerScore(this.player.ID)
      .subscribe((score) => {
        this.scoreSpan = score.getScore();
        this.navService.appCache.set(this.navService.scoreCacheKey, score);
      });
  }
}
