import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { AppService } from '../app.component/app.component.service';
import { Player } from '../player.component/player';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { ScoreObject } from './../player.component/score';
import { DataConstants } from 'virtualRoot/DataConstants';
@Injectable()
export class NavigationService {

  private readonly url: string = this.appService.url;
  private readonly headers: HttpHeaders = this.appService.headers;
  public readonly scoreCacheKey = DataConstants.cacheKeys.playerScoreKey;
  public readonly appCache = this.appService.getAppCache();
  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  getPlayerScore(playerID: number): Observable<ScoreObject> {
    const url = `${ this.url }/players/get/score`;
    const params = { 'id': `${ playerID }` };

    if (this.appCache.get(this.scoreCacheKey) !== undefined) {
      return of(this.appCache.get(this.scoreCacheKey));
    }

    return this.http.get<any>(url, { headers: this.headers, params: params })
      .flatMap(queryResult => {
        return this.deserializeScore(queryResult, playerID);
      });
  }

  deserializeScore(queryResult: any, ID: number): Observable<ScoreObject> {
    const obj: ScoreObject = queryResult.length === 0 ? new ScoreObject(0, 0) : new ScoreObject(queryResult[0].win, queryResult[0].loss);
    const player: Player = this.appService.getPlayers().find(p => p.ID === ID);
    player.score = obj;
    return of(player.score);
  }
}
