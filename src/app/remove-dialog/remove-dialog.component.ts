import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Player } from './../player.component/player';
import { Router } from '@angular/router';
import { AppService } from './../app.component/app.component.service';
import { DialogService } from './../dialog.component/dialog.service';
import { DialogComponent } from './../dialog.component/dialog.component';
import { RemoveDialogService } from './remove-dialog.service';
import { of } from 'rxjs/observable/of';
import { DataConstants } from '../DataConstants';
@Component({
  selector: 'bball-remove-dialog',
  templateUrl: './remove-dialog.component.html',
  styleUrls: ['./remove-dialog.component.css', './../css/dialog.css']
})
export class RemoveDialogComponent implements OnInit {

  public readonly dialogTitle: string = 'Remove From Today\'s PlayerList';
  public readonly message: string = 'Are you sure you\'d like to remove this player from today\'s player list?';
  public readonly playerTitle: string = 'Player';
  public player: Player;
  private dialogRef: MatDialogRef<DialogComponent, any>;

  constructor(
    private appService: AppService,
    private removeDialogService: RemoveDialogService,
    private dialogService: DialogService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dialogRef = this.dialogService.getDialogRef();
    this.player = this.dialogService.getPlayer();
  }
  onRemoveClicked(): void {
    this.removeDialogService.remove(this.player.ID)
      .flatMap(res => {
        if (res) {
          return of(true);
        }
        return of(false);
      })
      .subscribe((res) => {
        if (res) {
          alert('Let us know if you change your mind again!');
          this.dialogRef.close(res);
          return;
        }
        this.appService.navigateToComponent(DataConstants.pageNameConstantData.errorPage, false);
      });
  }

  onCancelClicked(): void {
    this.dialogRef.close(false);
  }
}
