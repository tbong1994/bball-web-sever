import { Component, OnInit, EventEmitter, Output, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ButtonService } from './button.service';
import { PlayerService } from './../player.component/player.service';
@Component({
  selector: 'bball-ngbd-buttons',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css', './../app.component/app.component.css']
})
export class NgbdButtonsRadioreactiveComponent implements OnInit {

  @ViewChild('yesButton') yesButtonElement: ElementRef;
  @Input() vote: string;
  @Output() answerEmitter = new EventEmitter<string>();
  public radioGroupForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private buttonService: ButtonService,
    private playerService: PlayerService
  ) { }

  ngOnInit() {
    this.radioGroupForm = this.formBuilder.group({
      'yesOrNo': 'undecided'
    });
    this.preSelectButtons();
  }

  buttonClicked(value: string): void {
    this.answerEmitter.emit(value);
  }

  preSelectButtons(): void {
    if (this.vote === 'yes') {
      this.yesButtonElement.nativeElement.checked = 'checked';
    }
  }
}
