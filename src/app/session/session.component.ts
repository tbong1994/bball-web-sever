import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from './../dialog.component/dialog.component';
import { DialogService } from './../dialog.component/dialog.service';
import { Player } from './../player.component/player';
import { AppService } from './../app.component/app.component.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bball-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
