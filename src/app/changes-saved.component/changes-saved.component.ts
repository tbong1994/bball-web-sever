import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from './../dialog.component/dialog.component';
import { LoginService } from './../login/login.service';
import { Player } from './../player.component/player';
import { DialogService } from './../dialog.component/dialog.service';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from '../DataConstants';
@Component({
  selector: 'bball-changes-saved',
  templateUrl: './changes-saved.component.html',
  styleUrls: ['./changes-saved.component.css', './../css/dialog.css', './../css/animation.css', './../css/font.css']
})
export class ChangesSavedComponent implements OnInit, AfterViewInit {

  public message: string;
  public todaysPlayersMessage: string;
  public readonly playerTitle: string = 'Player';
  public readonly okButtonString: string = 'ok';
  private dialogRef: MatDialogRef<DialogComponent, any>;
  public readonly dialogTitle: string = 'Changes Saved';
  public player: Player;
  public players: Player[];

  constructor(
    private dialogService: DialogService,
    private appService: AppService,
    private loginService: LoginService
  ) { }

  ngAfterViewInit() {
    this.dialogRef.disableClose = false;
  }
  ngOnInit() {
    this.dialogRef = this.dialogService.getDialogRef();
    this.player = this.loginService.getCurrentUser();
    this.setMessage();
  }

  goToHomepage(): void {
    const dialogData = new Map<string, any>();
    dialogData.set(DataConstants.dialogDataConstants.player, this.player);
    dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.changedSaved);

    this.dialogRef.close(dialogData);
  }

  setMessage(): void {
    this.message = 'See you on the court ' + this.player.firstname + '!';
    this.players = this.getPlayersWhoArePlayingToday();
    if (this.players.length < 1) {
      this.todaysPlayersMessage = 'No one else has voted yet';
    } else {
      this.todaysPlayersMessage = 'Today, we\'ve got you and ';
    }
  }

  getPlayersWhoArePlayingToday(): Player[] {
    return this.appService.getTodaysPlayers();
  }
}
