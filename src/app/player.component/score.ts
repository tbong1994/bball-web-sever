export class ScoreObject {
    constructor(win: number, loss: number) {
        this.win = win;
        this.loss = loss;
    }
    win: number;
    loss: number;
    getScore(): string {
        return '' + this.win + 'W - ' + this.loss + 'L'; // displayed as i.e. "10W - 9L"
    }
}
