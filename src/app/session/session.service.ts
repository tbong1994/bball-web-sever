import { Injectable } from '@angular/core';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from './../DataConstants';
import { Player } from '../player.component/player';
@Injectable()
/**
 * @class SessionService takes care of session handling.
 */
export class SessionService {

  private localStorage: Storage = window.localStorage;
  private userToken: string;
  private timeoutID;
  private currentPlayer: Player;
  constructor(
    private appService: AppService
  ) { }

  getLocalStorage(): Storage {
    return this.localStorage;
  }
  userActive(): void {
    if (this.localStorage.length > 0) {
      window.clearTimeout(this.timeoutID);
      this.setUserSessionTimer();
    }
  }
  updateCurrentUser(player: Player): void {
    this.currentPlayer = player;
  }
  getCurrentUser(): Player {
    return this.currentPlayer;
  }
  setUserSessionTimer(): void {
    const now = new Date();
    const lastUserActiveTime = now.getHours() + '%' + now.getMinutes();
    this.localStorage.setItem(DataConstants.localStorageDataConstants.userTokenExpirationKey, lastUserActiveTime);
  }

  getUserToken(): string {
    return this.userToken;
  }

  updateUserToken(token: string): void {
    this.userToken = token;
  }
}
