// 3RD PARTY MODULES
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CovalentLayoutModule } from '@covalent/core/layout';
import { CovalentStepsModule } from '@covalent/core/steps';

// THIRD PARTY
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// MODULES
import { AppRouterModule } from './app.router.module';
import { MatDialogModule } from '@angular/material';
// <Mat date picker dependencies>
import { MatDatepickerModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
// </Mat date picker dependencies>
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';


//DIRECTIVES
import { DraggableDirective } from './directives/draggable-directive/draggable.directive';

// COMPONENTS
import { NgbdButtonsRadioreactiveComponent } from './button.component/button.component';
import { AppComponent } from './app.component/app.component';
import { PlayerComponent } from './player.component/player.component';
import { SignUpComponent } from './signup.component/signup.component';
import { WelcomeComponent } from './welcome.component/welcome.component';
import { EditProfileComponent } from './edit-profile.component/edit-profile.component';
import { DialogComponent } from './dialog.component/dialog.component';
import { VoteComponent } from './vote.component/vote.component';
import { ChangesSavedComponent } from './changes-saved.component/changes-saved.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SignupDialogComponent } from './signup-dialog/signup-dialog.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { RemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { UpdateScoreComponent } from './update-score/update-score.component';
import { LastGameStatsComponent } from './last-game-stats/last-game-stats.component';
import { SessionComponent } from './session/session.component';
import { ChangePinComponent } from './change-pin/change-pin.component';

// SERVICES
import { AppService } from './app.component/app.component.service';
import { ButtonService } from './button.component/button.service';
import { PlayerService } from './player.component/player.service';
import { SignUpService } from './signup.component/signup.service';
import { WelcomeService } from './welcome.component/welcome.service';
import { EditProfileService } from './edit-profile.component/edit-profile.service';
import { DialogService } from './dialog.component/dialog.service';
import { VoteService } from './vote.component/vote.service';
import { LoginService } from './login/login.service';
import { DeleteAccountService } from './delete-account/delete-account.service';
import { ChangesSavedService } from './changes-saved.component/changes-saved.service';
import { RemoveDialogService } from './remove-dialog/remove-dialog.service';
import { UpdateScoreService } from './update-score/update-score.service';
import { LastGameStatsService } from './last-game-stats/last-game-stats.service';
import { DatePickerService } from './date-picker/date-picker.service';
import { SessionService } from './session/session.service';
import { ChangePinService } from './change-pin/change-pin.service';
import { NavigationService } from './navigation/navigation.service';


@NgModule({
  declarations: [
    AppComponent,
    NgbdButtonsRadioreactiveComponent,
    PlayerComponent,
    SignUpComponent,
    WelcomeComponent,
    EditProfileComponent,
    DialogComponent,
    VoteComponent,
    ChangesSavedComponent,
    LoginComponent,
    NavigationComponent,
    SignupDialogComponent,
    DeleteAccountComponent,
    RemoveDialogComponent,
    UpdateScoreComponent,
    LastGameStatsComponent,
    DatePickerComponent,
    ErrorPageComponent,
    SessionComponent,
    DraggableDirective,
    ChangePinComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AppRouterModule,
    MatDialogModule,
    // <Mat date picker dependencies>
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatNativeDateModule,
    MatInputModule,
    // </Mat date picker dependencies>
    BrowserAnimationsModule,
    //<Covalent Core Modules>
    CovalentLayoutModule,
    CovalentStepsModule,
    //</Covalent Core Modules>
    OverlayModule,
    PortalModule,
    NgbModule.forRoot(),
  ],
  providers: [AppService, ButtonService, PlayerService, SignUpService, VoteService, ChangesSavedService,
    WelcomeService, EditProfileService, DialogService, LoginService, DeleteAccountService, RemoveDialogService,
    UpdateScoreService, LastGameStatsService, DatePickerService, SessionService, ChangePinService, NavigationService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }
