import { TestBed, inject } from '@angular/core/testing';

import { ChangePinService } from './change-pin.service';

describe('ChangePinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangePinService]
    });
  });

  it('should be created', inject([ChangePinService], (service: ChangePinService) => {
    expect(service).toBeTruthy();
  }));
});
