import { Component, OnInit, Input, EventEmitter, Output, DebugElement } from '@angular/core';
import { MatFormField, MatInput } from '@angular/material';
import { MatIcon } from '@angular/material';
import { MatDatepickerInputEvent } from '@angular/material/datepicker'; // this allows user input through input box or the calendar to be detected

@Component({
  selector: 'bball-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
/**
 * @class Date picker control for picking dates. Mat datepicker control uses cdk overlay DOM element which is also used by mat dialog control. There is
 * no good way(scalable) to override the date picker's overlay backdrop for now. If there's a better date picker control or if there's a way to override this
 * mat date picker control where it can be used in other components without having to override everytime, fix it.
 */
export class DatePickerComponent implements OnInit {

  public myDate;
  public today = new Date();
  @Output() dateEmitter = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }
  dateEntered(date: string) {
    const dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    if (!dateFormat.test(date)) {
      //display something?
      return;
    }
    date = this.convertDateDisplayFormat(date);
    this.dateEmitter.emit(date);
  }

  convertDateDisplayFormat(date: string): string {
    const dateArray = date.split('/');
    let month: string, day: string, year: string;
    [month, day, year] = dateArray;
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return month + '-' + day + '-' + year;
  }
}
