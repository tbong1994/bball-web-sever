
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Player } from '../player.component/player';
import { ProfilePic } from './profilepic';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { AppService } from './../app.component/app.component.service';
import { MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from './../dialog.component/dialog.component';
@Injectable()
export class SignUpService {

    private readonly url: string = this.appService.url;
    private headers: HttpHeaders = this.appService.headers;
    private player: Player;
    private dialogRef: MatDialogRef<DialogComponent, any>;
    private profilePics$: Observable<ProfilePic[]>;
    constructor(
        private http: HttpClient,
        private appService: AppService
    ) { }

    register(player: Player): Observable<void> {
        const url = `${ this.url }/signup/create`;
        return this.http.post<void>(url, JSON.stringify(player), { headers: this.headers });
    }
    updateSignupPlayer(player: Player) {
        this.player = player;
    }
    getSignUpPlayer(): Player {
        return this.player;
    }

    getProfilePictures(): Observable<ProfilePic[]> {
        if (this.profilePics$ === undefined) {
            const url = `${ this.url }/signup/profilepictures`;
            this.profilePics$ = this.http.get<ProfilePic[]>(url, { headers: this.headers });
        }
        return this.profilePics$;
    }

    updateDialogRef(dialogRef: MatDialogRef<DialogComponent, any>): void {
        this.dialogRef = dialogRef;
    }

    getDialogRef(): MatDialogRef<DialogComponent, any> {
        return this.dialogRef;
    }

    getPlayers(): Player[] {
        return this.appService.getPlayers();
    }

    deserializeProfilePictures(profPics: any): Observable<ProfilePic[]> {
        const pics: ProfilePic[] = [];
        for (let i = 0; i < profPics.length; i++) {
            pics.push(new ProfilePic(profPics[i].imgurl, false));
        }
        return of(pics);
    }
}
