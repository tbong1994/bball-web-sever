import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './../app.component/app.component.service';
import { ChangePinService } from './change-pin.service';
import { DialogService } from './../dialog.component/dialog.service';
import { LoginService } from './../login/login.service';
import { Player } from './../player.component/player';
import { DataConstants } from '../DataConstants';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/observable';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from '../dialog.component/dialog.component';
@Component({
  selector: 'bball-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: ['./change-pin.component.css', '../css/dialog.css']
})
export class ChangePinComponent implements OnInit {

  public player: Player;
  public dialogTitle: string = 'Change Pin';
  public readonly message: string = 'enter new pin';
  public readonly playerTitle: string = 'Player';
  public displayErrorMessage: string;

  constructor(
    private appService: AppService,
    private changePinService: ChangePinService,
    private dialogService: DialogService,
    private loginService: LoginService,
    private router: Router,
    private dialogRef: MatDialogRef<DialogComponent>
  ) { }

  ngOnInit() {
    this.player = this.loginService.getCurrentUser();
    this.dialogRef = this.dialogService.getDialogRef();
  }

  save(pin: string): void {
    if (!this.appService.checkPinFormat(pin)) {
      this.displayErrorMessage = 'pin should consist of 4 digit numbers';
      return;
    }
    this.changePinService.updatePin(pin, this.player.ID)
      .flatMap((response) => {
        return of(response.isUpdated);
      })
      .catch((err) => this.appService.navigateToErrorPage(err, this.constructor.name))
      .subscribe((isSuccesful) => {
        if (isSuccesful) {
          localStorage.setItem(DataConstants.localStorageDataConstants.userTokenKey, '');
          alert('Pin changed successfully!');
          this.dialogRef.close();
        } else {
          this.displayErrorMessage = 'pin could not be updated';
        }
      });
  }

  back(): void {
    this.dialogService.navigateToComponent(DataConstants.dialogNames.editProfile, true);
  }
}
