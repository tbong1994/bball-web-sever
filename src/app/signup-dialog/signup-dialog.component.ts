import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Player } from './../player.component/player';
import { SignUpService } from './../signup.component/signup.service';
import { MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from './../dialog.component/dialog.component';

@Component({
  selector: 'bball-signup-dialog',
  templateUrl: './signup-dialog.component.html',
  styleUrls: ['./signup-dialog.component.css', './../css/dialog.css', './../css/font.css']
})
export class SignupDialogComponent implements OnInit {

  public readonly dialogMessage: string = 'Everything looks good?';
  public readonly dialogTitle: string = 'Signup';
  public player: Player;
  private dialogRef: MatDialogRef<DialogComponent, any>;

  constructor(
    private signupService: SignUpService
  ) { }

  ngOnInit() {
    this.player = this.signupService.getSignUpPlayer();
    this.dialogRef = this.signupService.getDialogRef();
  }

  save(): void {
    this.dialogRef.close(true);
  }

  back(): void {
    this.dialogRef.close(false);
  }
}
