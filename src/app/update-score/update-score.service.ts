import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from './../app.component/app.component.service';
import { Observable } from 'rxjs/observable';
import { LoginService } from './../login/login.service';
import { Player } from './../player.component/player';

@Injectable()
export class UpdateScoreService {

  private headers: HttpHeaders = this.appService.headers;
  private url: string = this.appService.url;

  constructor(
    private http: HttpClient,
    private appService: AppService,
    private loginService: LoginService
  ) { }

  getCurrentPlayer(): Player {
    return this.loginService.getCurrentUser();
  }

  updateScore(team1Score: string, team2Score: string, date: string): Observable<void> {
    const teamScores = { 'team1Score': parseInt(team1Score, 10), 'team2Score': parseInt(team2Score, 10), 'date': date };
    const url = `${ this.url }/score/update`;
    return this.http.put<void>(url, JSON.stringify(teamScores), { headers: this.headers });
  }
}
