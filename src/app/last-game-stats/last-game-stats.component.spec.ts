import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastGameStatsComponent } from './last-game-stats.component';

describe('LastGameStatsComponent', () => {
  let component: LastGameStatsComponent;
  let fixture: ComponentFixture<LastGameStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastGameStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastGameStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
