
import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Player } from './../player.component/player';
import { DialogService } from './dialog.service';
import { SessionService } from './../session/session.service';
import { Router } from '@angular/router';
import { Type } from '@angular/core/src/type';
import { DataConstants } from './../DataConstants';
@Component({
    selector: 'bball-dialog',
    templateUrl: 'dialog.html',
    styleUrls: ['./dialog.css', './../css/font.css', './../css/animation.css']
})
/**
 * @class When opening a dialog using this DialogComponent, dialog data should be passed in as a map.
 */
export class DialogComponent implements OnInit, AfterViewInit {

    public player: Player;
    public bodyDomElement: HTMLBodyElement;
    private currentComponentName: string;
    constructor(
        public dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Map<string, any>, // use inject decorator because MAT_DIALOG_DATA is not a typescript type
        private dialogService: DialogService,
        private sessionService: SessionService,
        private router: Router
    ) { }


    ngAfterViewInit() {
        this.bodyDomElement = this.dialogService.getApplicationBodyDOMElement();
        this.disableBackgroundScroll(this.bodyDomElement);
        this.dialogRef.beforeClose().subscribe((res) => {
            this.enableBackgroundScroll(this.bodyDomElement);
        });
        this.sessionService.setUserSessionTimer();
    }
    ngOnInit() {
        try {
            this.dialogService.updatePlayer(this.data.get(DataConstants.dialogDataConstants.player));
            this.dialogService.updateDialogType(this.data.get(DataConstants.dialogDataConstants.dialogName));
            this.dialogService.updateDialogRef(this.dialogRef);
            this.dialogService.updateDialogMessage(this.data.get(DataConstants.dialogDataConstants.displayMessage));
            this.dialogService.updateDialogData(this.data);
            this.openDialog(this.data.get(DataConstants.dialogDataConstants.dialogName));
        } catch (e) {
            console.log('dialog data is not a map object!');
            this.dialogRef.close();
            this.dialogService.navigateToErrorPage(e.message, this);
            return;
        }

    }

    onActivate(activeRouteComponent: any) {
        if (activeRouteComponent.dialogTitle !== undefined) {
            this.currentComponentName = activeRouteComponent.dialogTitle.toLowerCase().replace(/\s/g, '');
        }
    }

    close(): void {
        if (this.currentComponentName === 'changessaved') {
            const dialogData = new Map<string, any>();
            dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.changedSaved);
            this.dialogRef.close(dialogData);
        }
        this.dialogRef.close();
    }
    openDialog(dialogName: string): void {
        this.dialogService.navigateToComponent(dialogName, true);
    }
    disableBackgroundScroll(bodyDom: HTMLBodyElement): void {
        bodyDom.classList.add('scrollDisabled');
    }

    enableBackgroundScroll(bodyDom: HTMLBodyElement): void {
        bodyDom.classList.remove('scrollDisabled');
    }

    refreshPage(): void {
        window.location.reload();
    }
    userActivityDetected(): void {
        this.sessionService.userActive();
    }
}
