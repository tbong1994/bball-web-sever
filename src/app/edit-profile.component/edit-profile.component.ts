import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EditProfileService } from './edit-profile.service';
import { Player } from './../player.component/player';
import 'rxjs/add/operator/switchMap';
import { AsyncPipe } from '@angular/common';
import { ProfilePic } from './../signup.component/profilepic';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AppService } from './../app.component/app.component.service';
import { DialogComponent } from './../dialog.component/dialog.component';
import { DialogService } from './../dialog.component/dialog.service';
import { LoginService } from './../login/login.service';
import { DataConstants } from '../DataConstants';
@Component({
    selector: 'bball-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css', './../css/font.css', './../css/animation.css', './../css/dialog.css']
})
export class EditProfileComponent implements OnInit {

    public player: Player;
    public profilePictures: ProfilePic[] = [];
    public invalidUserInfoMessage: string;
    public isValidated: boolean;
    public lastChosenTeam: ProfilePic;
    public readonly saveWarningMsg: string = 'Are you sure you\'d like to save the changes?';
    private dialogRef: MatDialogRef<DialogComponent, any>;
    public readonly dialogTitle: string = 'Edit Profile';

    constructor(
        private router: Router,
        private editProfileService: EditProfileService,
        private loginService: LoginService,
        private dialogService: DialogService,
        private appService: AppService,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.player = this.loginService.getCurrentUser();
        this.getProfilePictures();
        this.dialogRef = this.dialogService.getDialogRef();
    }

    getProfilePictures(): void {
        this.editProfileService.getProfilePictures().subscribe(profpics => this.profilePictures = profpics);
    }

    save(firstname: string, lastname: string, email: string): void {
        firstname = firstname.toLowerCase().trim();
        lastname = lastname.toLowerCase().trim();
        email = email.toLowerCase().trim();

        this.isValidated = this.validateUserInfo(firstname, lastname, email);
        if (!this.isValidated) {
            /**
             * TODO: use mat dialog's input so that you don't have to manually update this error message when invalid input
             */
            this.invalidUserInfoMessage = 'Make sure you filled in first and last name, entered your hyland email, and picked a team';
            return;
        }

        this.editPlayerInfo(firstname, lastname, email);
        this.editProfileService.saveChanges(this.player).subscribe(() => {
            alert('Changes saved!');
            const dialogData = new Map<string, any>();
            dialogData.set(DataConstants.dialogDataConstants.player, this.player);
            dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.editProfile);
            this.dialogRef.close(dialogData);
        });
    }

    editPlayerInfo(firstname: string, lastname: string, email: string): void {
        this.player.setFirstName(firstname);
        this.player.setLastName(lastname);
        this.player.setEmail(email);
        this.player.setImageUrl(this.lastChosenTeam.getImgUrl().toLowerCase().trim());
    }

    onProfilePicClicked(pic: ProfilePic): void {
        if (this.lastChosenTeam === pic) {
            return;
        }
        if (this.lastChosenTeam !== undefined) {
            this.lastChosenTeam.setImgClicked(false);
        }
        pic.setImgClicked(true);
        this.lastChosenTeam = pic;
    }

    onChangePinClicked(): void {
        this.dialogService.navigateToComponent(DataConstants.dialogNames.changePinPage, true);
    }
    validateUserInfo(firstname: string, lastname: string, email: string): boolean {
        return this.appService.validateUserInfo(firstname, lastname, email, this.lastChosenTeam);
    }

    goToNavigationPage(): void {
        this.dialogService.navigateToNavigationComponent();
    }
}
