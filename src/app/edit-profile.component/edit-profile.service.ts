import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Player } from '../player.component/player';
import { AppService } from './../app.component/app.component.service';
import { SignUpService } from './../signup.component/signup.service';
import { PlayerService } from './../player.component/player.service';
import { ProfilePic } from './../signup.component/profilepic';
@Injectable()
export class EditProfileService {

    private readonly url: string = this.appService.url;
    private headers: HttpHeaders = this.appService.headers;
    private player: Player;

    constructor(
        private http: HttpClient,
        private router: Router,
        private appService: AppService,
        private signupSerivce: SignUpService,
        private playerService: PlayerService
    ) { }

    /*TODO: change this to a subject! */
    getProfilePictures(): Observable<ProfilePic[]> {
        return this.signupSerivce.getProfilePictures()
            .flatMap(this.signupSerivce.deserializeProfilePictures);
    }

    saveChanges(editedPlayer: Player): Observable<void> {
        const params = { 'id': `${ editedPlayer.ID }` };
        const url = `${ this.url }/players/edit`;
        return this.http.put<void>(url, JSON.stringify(editedPlayer), { headers: this.headers, params: params });
    }
}
