import { TestBed, inject } from '@angular/core/testing';

import { DraggableDirectiveService } from './draggable-directive.service';

describe('DraggableDirectiveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DraggableDirectiveService]
    });
  });

  it('should be created', inject([DraggableDirectiveService], (service: DraggableDirectiveService) => {
    expect(service).toBeTruthy();
  }));
});
