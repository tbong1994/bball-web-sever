import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from '../DataConstants';
import { DialogService } from './../dialog.component/dialog.service';
@Component({
  selector: 'bball-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css', './../css/font.css']
})
export class ErrorPageComponent implements OnInit {

  public errorDisplayMessage: string;
  constructor(
    private router: Router,
    private appService: AppService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    // when navigated to error page, all dialogs should be closed
    const dialogRef = this.dialogService.getDialogRef() || undefined;
    if (dialogRef) {
      dialogRef.close();
    }
    const errorData = this.appService.getError();
    const errorMessage = errorData.get(DataConstants.errorDataConstants.errorMessage) || '';
    const errorComponentName = errorData.get(DataConstants.errorDataConstants.errorComponentName) || '';

    if (errorMessage === '' && errorComponentName === '') {
      this.navigateToHome(); // no error. should not be here
      return;
    }
    this.errorDisplayMessage = errorMessage + ' from ' + errorComponentName;
  }
  navigateToHome(): void {
    try {
      this.appService.navigateToComponent(DataConstants.pageNameConstantData.homePage, false);
    } catch (e) {
      console.log(e);
    }
  }

}
