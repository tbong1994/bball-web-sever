import { Injectable } from '@angular/core';
import { Player } from './../player.component/player';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.component/app.component.service';
@Injectable()
export class DeleteAccountService {

  private url: string = this.appService.url;
  private headers: HttpHeaders = this.appService.headers;
  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  delete(player: Player): Observable<void> {
    const params = { 'id': `${ player.ID }` };
    const url = `${ this.url }/players/delete`;
    return this.http.delete<void>(url, { headers: this.headers, params: params });
  }
}
