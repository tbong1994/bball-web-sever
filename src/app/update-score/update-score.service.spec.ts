import { TestBed, inject } from '@angular/core/testing';

import { UpdateScoreService } from './update-score.service';

describe('UpdateScoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateScoreService]
    });
  });

  it('should be created', inject([UpdateScoreService], (service: UpdateScoreService) => {
    expect(service).toBeTruthy();
  }));
});
