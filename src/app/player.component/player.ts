import { ScoreObject } from 'virtualRoot/player.component/score';

export class Player {
  ID: number;
  firstname: string;
  lastname: string;
  email: string;
  pin: string; // pw
  imgurl: string; // profile pic file URL
  answer: number; // player's vote
  answerdate: string; // last voted date
  rating: number; // rating which changes based on player's win
  last_played_date: string;
  team: number;
  isAdmin: boolean; // admin flag
  height: number;
  speed: number;
  score: ScoreObject; // player's score history
  stats: number; // rating, but does not increase over time

  constructor(firstname: string, lastname: string, email: string, imgurl: string) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.imgurl = imgurl;
  }
  setID(ID: number): void {
    this.ID = ID;
  }
  setAdmin(isAdmin: any): void {
    if (isAdmin === 1) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }
  setAnswer(answer: number): void {
    this.answer = answer;
  }
  setAnswerDate(date: string): void {
    this.answerdate = date;
  }
  setFirstName(fname: string): void {
    this.firstname = fname;
  }
  setLastName(lname: string): void {
    this.lastname = lname;
  }
  setEmail(email: string): void {
    this.email = email;
  }
  setImageUrl(imgUrl: string): void {
    this.imgurl = imgUrl;
  }
  setPin(pin: string): void {
    this.pin = pin;
  }
  setHeight(height: number): void {
    this.height = height;
  }
  setSpeed(speed: number): void {
    this.speed = speed;
  }
  setScore(score: ScoreObject): void {
    this.score = score;
  }
  setRating(rating: number): void {
    this.rating = rating;
  }
  setStats(stats: number): void {
    this.stats = stats;
  }
  setTeam(team: number): void {
    this.team = team;
  }
}
