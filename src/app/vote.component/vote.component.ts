import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { Player } from './../player.component/player';
import { PlayerService } from './../player.component/player.service';
import { DialogService } from './../dialog.component/dialog.service';
import { Location } from '@angular/common';
import { LoginService } from './../login/login.service';
import { MatDialogRef } from '@angular/material';
import { DataConstants } from './../DataConstants';
import { DialogComponent } from './../dialog.component/dialog.component';
import { VoteService } from './vote.service';
import { AppService } from '../app.component/app.component.service';
@Component({
    selector: 'bball-vote',
    templateUrl: './vote.component.html',
    styleUrls: ['./vote.component.css', './../css/dialog.css', './../css/font.css', './../css/animation.css']
})
export class VoteComponent implements OnInit, AfterViewInit {

    public player: Player;
    public readonly message: string = 'You should select either yes or no before saving';
    private dialogRef: MatDialogRef<DialogComponent, any>;
    public readonly dialogTitle: string = 'vote';
    public isVoteSelected: boolean;
    public userVote: string;
    public isVoteAllowed: boolean;
    public readonly voteNotAllowedSpan: string = 'Voting for today has closed';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private appService: AppService,
        private playerService: PlayerService,
        private dialogService: DialogService,
        private location: Location,
        private voteService: VoteService,
        private loginService: LoginService
    ) { }

    ngOnInit() {
        this.player = this.loginService.getCurrentUser();
        this.dialogRef = this.dialogService.getDialogRef();
        this.isVotingAllowed()
            .flatMap((isAllowed) => {
                if (isAllowed) {
                    this.isVoteAllowed = true;
                    return of(true);
                }
                this.isVoteAllowed = false;
                return of(false);
            })
            .catch((err) => {
                this.appService.navigateToErrorPage(err.message, this.constructor.name);
                return of(false);
            })
            .subscribe();
    }
    ngAfterViewInit() {
        //when backdrop is clicked, don't close the dialog
    }

    onVoteClicked(vote: string) {
        this.isVoteSelected = true;
        this.userVote = vote;
    }
    save(): void {
        if (this.isVotingAllowed()) {
            if (this.userVote !== '') { // vote value could either be an empty string or a 'yes'
                this.voteService.save(this.player)
                    .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
                    .subscribe(res => {
                        this.appService.navigateToComponent(DataConstants.dialogNames.changedSaved, true);
                    });
            } else {
                this.isVoteSelected = false;
            }
        }
    }

    isVotingAllowed(): Observable<boolean> {
        let votingAllowed = false;

        const time = JSON.parse(this.voteService.getCurrentTime());
        const hourKey = DataConstants.hourConstantData.hour;
        const minuteKey = DataConstants.hourConstantData.mins;
        const { hourKey: hour, minuteKey: minutes } = time;

        if (hour < 15 || hour === 15 && minutes < 30) { // hour is in military time
            votingAllowed = true;
        }

        return of(votingAllowed);
    }
}
