import { Component, OnInit } from '@angular/core';
import { PlayerAndScoreElement } from './player-and-score';
import { LastGameStatsService } from './last-game-stats.service';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/observable';
import { AppService } from '../app.component/app.component.service';
import { Router } from '@angular/router';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { DataConstants } from '../DataConstants';
@Component({
  selector: 'bball-last-game-stats',
  templateUrl: './last-game-stats.component.html',
  styleUrls: ['./last-game-stats.component.css', './../css/dialog.css', './../css/font.css']
})
export class LastGameStatsComponent implements OnInit {

  public scoreFromLastGameSpan: string;
  public lastGameScoreColon: string;
  public Team1Span: string;
  public Team2Span: string;
  public lastGamePlayedDate: string;
  public lastTeam1Score: number;
  public lastTeam2Score: number;
  public lastTeam1: PlayerAndScoreElement[];
  public lastTeam2: PlayerAndScoreElement[];

  constructor(
    private appService: AppService,
    private lastGameService: LastGameStatsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.lastGameService.getLastGameScore()
      .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
      .subscribe((elemList: PlayerAndScoreElement[]) => {
        this.initializeUI(elemList);
      });
  }

  initializeUI(elemList: PlayerAndScoreElement[]): void {
    this.scoreFromLastGameSpan = '';
    if (elemList.length > 0) {
      this.lastGameScoreColon = ' : ';
      this.scoreFromLastGameSpan = 'score from last game ';
      this.Team1Span = 'Team 1';
      this.Team2Span = 'Team 2';
      this.lastGamePlayedDate = elemList[0].getDate() || '';
      /**
       * TODO: Change this logic to generate teams based on their team number, because there may be 3 vs 4 games, etc. in the future.
       */
      this.lastTeam1 = elemList.slice(0, elemList.length / 2) || [];
      this.lastTeam2 = elemList.slice(elemList.length / 2, elemList.length) || [];
      this.lastTeam1Score = this.lastTeam1[0].getScore();
      this.lastTeam2Score = this.lastTeam2[0].getScore();
    }
  }
}
