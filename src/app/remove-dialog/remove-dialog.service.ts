import { Injectable } from '@angular/core';
import { Player } from './../player.component/player';
import { Observable } from 'rxjs/observable';
import { AppService } from './../app.component/app.component.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/mergeMap';
import { of } from 'rxjs/observable/of';

@Injectable()
export class RemoveDialogService {

  private url = this.appService.url;
  private headers: HttpHeaders = this.appService.headers;
  constructor(
    private appService: AppService,
    private http: HttpClient
  ) { }

  remove(playerID: number): Observable<boolean> {
    const params = { 'id': `${ playerID }` };
    const url = `${ this.url }/players/remove-from-todays-playlist`;
    return this.http.delete<boolean>(url, { headers: this.headers, params: params }).flatMap(res => {
      if (res) { // result is an array of OK packets if success
        return of(true);
      }
      return of(false);
    });
  }
}
