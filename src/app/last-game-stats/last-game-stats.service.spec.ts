import { TestBed, inject } from '@angular/core/testing';

import { LastGameStatsService } from './last-game-stats.service';

describe('LastGameStatsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LastGameStatsService]
    });
  });

  it('should be created', inject([LastGameStatsService], (service: LastGameStatsService) => {
    expect(service).toBeTruthy();
  }));
});
