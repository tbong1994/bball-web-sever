import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppService } from './../app.component/app.component.service';
import { of } from 'rxjs/observable/of';
import { Player } from './../player.component/player';
import { PlayerAndScoreElement } from './player-and-score';
@Injectable()
export class LastGameStatsService {

  private url: string = this.appService.url;
  private headers: HttpHeaders = this.appService.headers;

  constructor(
    private appService: AppService,
    private http: HttpClient
  ) { }

  getLastGameScore(): Observable<PlayerAndScoreElement[]> {
    const url = `${ this.url }/score/last/game`;
    return this.http.get<PlayerAndScoreElement[]>(url, { headers: this.headers })
      .flatMap(this.parseScoreTables);
  }

  parseScoreTables(scoreTable: any): Observable<PlayerAndScoreElement[]> { //scoreTable is in row data packet format.
    const playerAndScoreElementList: PlayerAndScoreElement[] = [];
    if (scoreTable.length > 0) {
      for (let i = 0; i < scoreTable.length; i++) {
        playerAndScoreElementList.push(
          new PlayerAndScoreElement(scoreTable[i].firstname, scoreTable[i].team, scoreTable[i].score, scoreTable[i].date)
        );
      }
    }
    return of(playerAndScoreElementList);
  }
}
