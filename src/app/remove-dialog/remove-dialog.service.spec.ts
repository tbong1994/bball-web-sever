import { TestBed, inject } from '@angular/core/testing';

import { RemoveDialogService } from './remove-dialog.service';

describe('RemoveDialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoveDialogService]
    });
  });

  it('should be created', inject([RemoveDialogService], (service: RemoveDialogService) => {
    expect(service).toBeTruthy();
  }));
});
