import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';
import { Player } from '../player.component/player';
import { PlayerService } from './../player.component/player.service';
import { SignUpService } from './signup.service';
import { ProfilePic } from './profilepic';
import { DialogComponent } from './../dialog.component/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from '../DataConstants';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
@Component({
    selector: 'bball-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css', './../css/font.css', './../css/animation.css']
})
export class SignUpComponent implements OnInit {

    private player: Player;
    private players: Player[];
    public invalidUserInfoMessage: string;
    public isValidated: boolean;
    public lastChosenTeam: ProfilePic;
    public profilePictures: ProfilePic[]; // Profile pictures are NBA team pictures
    public readonly choosePictureString: string = 'Pick a team!'; // this team picture will be the player's profile picture
    public readonly registerString: string = 'Are you sure all the information is correct?';
    public teamPicked: boolean;
    public emailFormControl = new FormControl('', [
        Validators.required
    ]);
    public matcher = new MyErrorStateMatcher();
    constructor(
        private appService: AppService,
        private signupService: SignUpService,
        private playerService: PlayerService,
        private router: Router,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.getProfilePictures();
    }

    register(firstname: string, lastname: string, email: string, pin: string): void {
        if (this.lastChosenTeam === undefined) {
            return;
        }
        firstname = firstname.toLowerCase().trim();
        lastname = lastname.toLowerCase().trim();
        email = email.toLowerCase().trim();
        const pic = this.lastChosenTeam.getImgUrl().toLowerCase().trim();

        this.isValidated = this.validateUserAccount(firstname, lastname, email, pin);
        if (!this.isValidated) {
            this.invalidUserInfoMessage = 'Make sure you filled in first and last name, entered your hyland email, and four digit numbers for pin';
            return;
        }
        if (this.isValidated) {
            this.player = new Player(firstname, lastname, email, pic);
            this.player.setPin(pin);
            this.signupService.updateSignupPlayer(this.player);

            const dialogRef = this.displayDialog(this.player, this.registerString, pin);
            this.signupService.updateDialogRef(dialogRef);
            dialogRef.afterClosed().subscribe((result) => { // use code block to retain 'this'
                this.onDialogClose(result);
            });
        }
    }

    onDialogClose(answer: any): void {
        if (answer) {
            this.signupService.register(this.player)
                .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
                .subscribe(
                    res => {
                        alert('Player Successfully Created!');
                        this.appService.navigateToComponent(DataConstants.pageNameConstantData.homePage, false);
                    }
                );
        }
    }

    displayDialog(player: Player, msg: string, pin: string): MatDialogRef<DialogComponent, any> {
        const dialogData = new Map<string, any>();
        dialogData.set(DataConstants.dialogDataConstants.player, this.player);
        dialogData.set(DataConstants.dialogDataConstants.displayMessage, msg);
        dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.signUp);
        dialogData.set(DataConstants.dialogDataConstants.userPin, pin);
        const dialogRef = this.dialog.open(DialogComponent, {
            data: dialogData
        });
        return dialogRef;
    }

    validateUserAccount(firstname: string, lastname: string, email: string, pin: string): boolean {
        if (this.checkUserAlreadyExists(firstname, lastname)) {
            this.invalidUserInfoMessage = 'a user already exists with the same name';
            return false;
        }
        return this.appService.validateUserInfo(firstname, lastname, email, this.lastChosenTeam)
            && this.appService.checkPinFormat(pin);
    }

    checkUserAlreadyExists(firstname: string, lastname: string): boolean {
        this.players = this.signupService.getPlayers() || undefined;
        if (this.players !== undefined) {
            for (let i = 0; i < this.players.length; i++) {
                if (this.players[i].firstname === firstname && this.players[i].lastname === lastname) {
                    return true;
                }
            }
        }
        return false;
    }
    onHomeClicked(): void {
        this.appService.navigateToComponent(DataConstants.pageNameConstantData.homePage, false);
    }

    onProfilePicClicked(pic: ProfilePic): void {
        this.teamPicked = true;
        if (this.lastChosenTeam !== undefined) {
            this.lastChosenTeam.setImgClicked(false);
        }
        this.lastChosenTeam = pic;
        this.lastChosenTeam.setImgClicked(true);
    }

    getProfilePictures(): void {
        this.signupService.getProfilePictures()
            .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
            .flatMap(this.signupService.deserializeProfilePictures)
            .subscribe(profpics => this.profilePictures = profpics);
    }
}
