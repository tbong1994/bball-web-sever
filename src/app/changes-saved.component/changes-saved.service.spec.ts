import { TestBed, inject } from '@angular/core/testing';

import { ChangesSavedService } from './changes-saved.service';

describe('ChangesSavedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangesSavedService]
    });
  });

  it('should be created', inject([ChangesSavedService], (service: ChangesSavedService) => {
    expect(service).toBeTruthy();
  }));
});
