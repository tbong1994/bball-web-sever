
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Player } from './../player.component/player';
import { DataConstants } from './../DataConstants';
import { ProfilePic } from '../signup.component/profilepic';

// use Injectable for dependency injection. All angular decorators tell Angular of the class's dependencies.
// However, services like this class do not require the any angular decorator, but it has dependencies, http, router, etc.
// that's why @Injectable() is required for this class and all other service classes, to tell Angular that they need their dependencies before initializing this component.
@Injectable()
/**
 * @class AppService is a global Service. It should not import any components or other services because other components/services will import this service.
 */
export class AppService {
    public headers: HttpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });
    public readonly url: string = 'http://dev-029204/hba-prod-app';
    public bodyDOMElement: HTMLBodyElement;
    private players: Player[];
    private todaysPlayers: Player[];
    private errorMessage: string;
    private errorComponentName: any;
    private readonly appCache: Map<string, any> = new Map<string, any>();
    constructor(
        private router: Router,
    ) { }

    setBodyDOMElement(bodyDOM: HTMLBodyElement) {
        this.bodyDOMElement = bodyDOM;
    }
    getBodyDOMElement(): HTMLBodyElement {
        return this.bodyDOMElement;
    }
    getTodaysDate(): string {
        const today = new Date();
        const day = today.getDate();
        const month = today.getMonth() + 1;
        const year = today.getFullYear();
        let monthAsString: string;
        let dayAsString: string;
        month < 10 ? monthAsString = '0' + month : monthAsString = '' + month; //monthAsString is to suppress typescript warning
        day < 10 ? dayAsString = '0' + day : dayAsString = '' + day;
        return monthAsString + '-' + dayAsString + '-' + year;
    }
    getCurrentTime(): string {
        const today = new Date();
        const hour = today.getHours();
        const mins = today.getMinutes();

        const hourKey: string = DataConstants.hourConstantData.hour;
        const minuteKey: string = DataConstants.hourConstantData.mins;

        const time = JSON.stringify({ hourKey: hour, minuteKey: mins });
        return time;
    }
    updatePlayers(players: Player[]): void {
        this.players = players || [];
    }
    updateTodaysPlayers(players: Player[]): void {
        this.todaysPlayers = players || [];
    }
    getTodaysPlayers(): Player[] {
        return this.todaysPlayers;
    }
    getPlayers(): Player[] {
        return this.players;
    }
    refreshPage(): void {
        window.location.reload();
    }
    /**
     * @param componentName page to navigate to.
     * @param isDialog indication whether the page is a dialog component page or not.
     * @returns a boolean observable; true if successfully navigated, false otherwise.
     */
    navigateToComponent(componentName: string, isDialog: boolean): Observable<boolean> {
        try {
            if (isDialog) {
                this.router.navigate([{ outlets: { dialogOutlet: [`${ componentName }`] } }]);
            } else {
                this.router.navigate([`${ componentName }`]);
            }
        } catch (e) {
            console.log(e.message);
            return of(false);
        }
        return of(true);
    }

    navigateToErrorPage(errorMessage: string, errorComponentName: string): Observable<boolean> {
        this.updateErrorMessage(errorMessage, errorComponentName);
        try {
            this.navigateToComponent(DataConstants.pageNameConstantData.errorPage, false);
        } catch (e) {
            console.log(e.message);
            return of(false);
        }
        return of(true);
    }

    updateErrorMessage(errorMessage: string, errorComponentName: string): void {
        this.errorMessage = errorMessage || '';
        this.errorComponentName = errorComponentName || '';
    }

    getError(): Map<string, string> {
        const errorMap = new Map<string, string>();
        errorMap.set(DataConstants.errorDataConstants.errorMessage, this.errorMessage);
        errorMap.set(DataConstants.errorDataConstants.errorComponentName, this.errorComponentName);
        return errorMap;
    }

    validateUserInfo(firstname: string, lastname: string, email: string, lastChosenTeam: ProfilePic): boolean {
        return this.checkIfTeamChosen(lastChosenTeam) && this.checkNameFormat(firstname, lastname)
            && this.checkEmailFormat(email);
    }
    checkIfTeamChosen(team: ProfilePic): boolean {
        if (team === null || team === undefined) {
            return false;
        }
        return true;
    }
    checkNameFormat(firstname: string, lastname: string): boolean {
        if (firstname.length === 0 || lastname.length === 0) {
            return false;
        }
        if (!/^[A-Za-z0-9'"*_@./#&$!?\s]+$/.test(firstname) ||
            !/^[A-Za-z0-9'"*_@./#&$!?\s]+$/.test(lastname)) { // name contains nonalphabet characters
            return false;
        }
        return true;
    }
    checkEmailFormat(email: string): boolean {
        if (!/^\w+([\.-]?\w+)*@hyland.com$/.test(email)) {
            return false;
        }
        return true;
    }
    checkPinFormat(pin: string): boolean {
        if (!/^\d{4}$/.test(pin)) {
            return false;
        }
        return true;
    }
    getAppCache(): Map<string, any> {
        return this.appCache;
    }
    addToCache(cacheKey: string, cacheContent: any): void {
        this.appCache.set(cacheKey, cacheContent);
    }
}
