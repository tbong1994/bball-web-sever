import { AfterViewInit, Directive, ElementRef, Input, NgZone, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/takeUntil';
import { Subject } from 'rxjs/Subject';
import { AppService } from 'virtualRoot/app.component/app.component.service';
/**
 * TODO: this draggable directive is not yet used anywhere, because mat-dialog is dynamically generated and this directive requires a
 * drag handle prior to setting this directive to the drag handle. Since mat-dialog is dyanmically generated, and most of the dialogs are routed
 * via router outlet in this project, it's hard to apply this directive to an HTML Element that does not yet exist at the time.
 * Need to figure out a way to apply this directive in the right place so that this
 * directive is applied as minimal as possible(router outlet or something).
 * 1 Option is to ignore input boxes or buttons for drag target, but not yet known how to do that.
 * 1 other option is to set the drag handle as something apart from the router outlet so that it's generated before this directive is applied.
 */
@Directive({
  selector: '[bballDraggable]'
})
export class DraggableDirective implements AfterViewInit, OnDestroy {

  @Input() dragHandleElement: string; // if not specified by the consumer, the current DOM element will be used as the handle
  @Input() dragTargetElement: string; // element to be dragged

  // draggable element
  private targetElement: HTMLElement;

  // handle element for dragging
  private handleElement: HTMLElement;

  /**
   * @prop element position coordiate to keep track of dragging movement from point A to point B
   */
  private delta = { x: 0, y: 0 };

  /**
   * @prop element position coordiate with respect to current view
   */
  private offset = { x: 0, y: 0 };

  private destroy$ = new Subject<void>();

  ngAfterViewInit(): void {
    this.initialize();
  }

  initialize(): void {
    try {
      this.setupDragTarget();
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  setupDragTarget(): void {
    this.handleElement = this.dragHandleElement ? document.querySelector(this.dragHandleElement) as HTMLElement : this.elementRef.nativeElement;
    this.targetElement = document.querySelector(this.dragTargetElement) as HTMLElement; // querySelector(selector) returns the 1st element from the DOM that matches the selector
    this.setupEvents();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
  constructor(
    private elementRef: ElementRef,
    private zone: NgZone,
    private appService: AppService
  ) { }

  private setupEvents(): void {
    // since all mouse events for dragging are observables, if we run this inside angular's zone, it would run change detection everytime.
    // which would be a catastrophic(maybe not) bottleneck. So run this event listeners outside of angular zone for better performance.
    this.zone.runOutsideAngular(() => {
      // fromEvent(element, event) returns an observable after binding an element to an event
      let mousedown$;
      let mousemove$;
      let mouseup$;
      try {
        mousedown$ = Observable.fromEvent(this.handleElement, 'mousedown');
        mousemove$ = Observable.fromEvent(this.handleElement, 'mousemove');
        mouseup$ = Observable.fromEvent(this.handleElement, 'mouseup');
      } catch (e) {
        this.appService.navigateToErrorPage(e, this.constructor.name);
      }


      //mousedrag$ is an observable that is returned by mousedown$, which returns mousemove$. so whoever subscribes to mousedrag$ would be subscribing to mousemove$ observable
      const mousedrag$ = mousedown$.switchMap((mouseDownEvent: MouseEvent) => {
        const startX = mouseDownEvent.clientX; // initial mousedown event coordiate x with respect to current view
        const startY = mouseDownEvent.clientY; // initial mousedown event coordiate y with respect to current view

        // mousemove$ is only invoked after mousedown$ has been invoked
        return mousemove$.map((mouseNewPositionEvent: MouseEvent) => { // now enter this when mousemove is invoked
          event.preventDefault();
          this.delta = { // compare target element coordiate differences from mousedown to mousemove
            x: mouseNewPositionEvent.clientX - startX,
            y: mouseNewPositionEvent.clientY - startY
          };
        }).takeUntil(mouseup$); // takeUntil returns values from the source observable(mousemove$) until the target observable(mouseup$) returns
        // which means when mouseup$ event is invoked, mousemove$ will stop emitting values
        // and until mouseup$ event is invoked, values will keep being emitted to mousedrag$.subscribe().
      }).takeUntil(this.destroy$);

      mousedrag$.subscribe(() => {
        if (this.delta.x === 0 && this.delta.y === 0) { // if the position of the dialog has not been changed, leave it
          return;
        }
        this.moveElement(); // position has changed
      });

      //invoked when mouse up event is invoked
      mouseup$.takeUntil(this.destroy$).subscribe(() => {
        this.offset.x += this.delta.x;
        this.offset.y += this.delta.y;
        this.delta = { x: 0, y: 0 }; // reset delta as current position of the element should alwasy be the starting position
      });
    });
  }

  private moveElement(): void { // moves the element by using the css transform property and translate method
    requestAnimationFrame(() => {
      this.targetElement.style.transform = `
      translate(${this.offset.x + this.delta.x }px,
                ${this.offset.y + this.delta.y }px)
      `;
    });
  }
}
