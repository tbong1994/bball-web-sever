import { Injectable } from '@angular/core';
import { AppService } from './../app.component/app.component.service';
import { Player } from './../player.component/player';
@Injectable()
export class ChangesSavedService {

  constructor(
    private appService: AppService
  ) { }

  getPlayers(): Player[] {
    return this.appService.getPlayers();
  }
}
