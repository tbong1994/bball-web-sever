import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import { NgClass } from '@angular/common';
import { Router } from '@angular/router';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Player } from '../player.component/player';
import { PlayerService } from './player.service';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './../dialog.component/dialog.component';
import { AppService } from './../app.component/app.component.service';
import { DataConstants } from '../DataConstants';

@Component({
    selector: 'bball-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.css', './../css/font.css', './../css/animation.css']
})

export class PlayerComponent implements OnInit {

    public players: Player[];
    public player: Player;
    public alternatePlayer: Player;
    public playersPlayingToday: Player[];
    public numberOfPlayersToday: number;
    public team1: Player[];
    public team2: Player[];
    public playerName: string = 'Select Player';
    public playingTodayString: string = 'loading...';
    private readonly loginString: string = 'Enter your pin to login!';
    private readonly minNumOfPlayersForAGame = 6;

    constructor(
        private playerService: PlayerService,
        private router: Router,
        private dialog: MatDialog,
        private appService: AppService,
        config: NgbDropdownConfig
    ) {
        config.placement = 'bottom-left';
        config.autoClose = true;
    }

    ngOnInit() {
        this.getLastScoreUpdatedTime().subscribe((date) => {
            if (date === this.getTodaysDate()) { // if score has been updated for today only load player list
                this.playingTodayString = 'See ya tomorrow';
                return this.playerService.getPlayers()
                    .flatMap(players => {
                        return this.initializePlayerList(players);
                    })
                    .subscribe(res => { console.log('score has been updated for today!'); });
            }
            // if score has not been updated for today, load normally
            this.getPlayers();
        });
    }
    initializePlayerList(players: Player[]): Observable<Player[]> {
        this.players = players || [];
        this.setPlayersVote(this.players);
        this.playerService.updatePlayers(players);
        return of(this.players);
    }
    initializeTeams(playersToday: Player[]): Observable<boolean> {
        this.resetTeams();
        if (playersToday.length >= this.minNumOfPlayersForAGame) {
            this.generateTeams(playersToday);
        }
        return of(true);
    }
    initializeTodaysPlayersList(players: Player[]): Observable<Player[]> {
        const todaysDate = this.getTodaysDate();
        this.playersPlayingToday = players.filter(player => {
            return player.answer === 1 && player.answerdate === todaysDate;
        });
        this.appService.updateTodaysPlayers(this.playersPlayingToday);
        this.numberOfPlayersToday = this.playersPlayingToday.length || 0;
        this.appendNumberOfPlayers(this.numberOfPlayersToday);
        return of(this.playersPlayingToday);
    }
    onDropDownMenuClicked(playerList: HTMLDivElement): void {
        playerList.scrollIntoView();
    }
    getPlayers(): void {
        this.playerService.getPlayers()
            .flatMap(players => {
                return this.initializePlayerList(players);
            })
            .flatMap(players => {
                return this.initializeTodaysPlayersList(players);
            })
            .flatMap(playersToday => {
                return this.initializeTeams(playersToday);
            })
            .catch((err) => {
                this.appService.navigateToErrorPage(err.message, this.constructor.name);
                return of(false);
            })
            .subscribe(result => console.log('player component initialization successful? ' + result));
    }
    resetTeams(): void { // redefine 7th player and teams
        this.alternatePlayer = undefined;
        this.team1 = [];
        this.team2 = [];
    }

    generateTeams(players: Player[]): void {
        const sortedPlayers = this.sortPlayers(players);
        if (sortedPlayers.length > this.minNumOfPlayersForAGame && sortedPlayers.length % 2 !== 0) {
            this.getSeventhPlayerObservable().subscribe((alternatePlayerData) => {
                const playerID: number = parseInt(alternatePlayerData.playerID, 10);
                [this.alternatePlayer] = this.playersPlayingToday.filter(player => player.ID === playerID);
                const sortedPlayersWithoutAltPlayer = sortedPlayers.filter((player) => player !== this.alternatePlayer); // exclude  alt player from sorted players
                this.dividePlayers(sortedPlayersWithoutAltPlayer); // dividePlayers() in subscribe() because this is asynchronous
            });
        } else {
            this.dividePlayers(sortedPlayers); // if no 7th player, just divide players right away
        }
    }

    /**
     * @function dividePlayers() generates 2 teams and updates the teams in db
     * @param sortedPlayers list of players sorted by height
     */
    dividePlayers(sortedPlayers: Player[]): void {
        let team1RatingSum: number = 0;
        let team2RatingSum: number = 0;
        const team1: number = 1, team2: number = 2;
        for (let sortedPlayerIndex = 0; sortedPlayerIndex < sortedPlayers.length; sortedPlayerIndex++) {
            const currentPlayer = sortedPlayers[sortedPlayerIndex];
            if (team1RatingSum <= team2RatingSum) {
                team1RatingSum += currentPlayer.stats;
                this.team1.push(currentPlayer);
                currentPlayer.setTeam(team1);
            } else if (team1RatingSum > team2RatingSum) {
                team2RatingSum += currentPlayer.stats;
                this.team2.push(currentPlayer);
                currentPlayer.setTeam(team2);
            }
        }
        this.updatePlayersTeams(this.team1, this.team2);
    }

    updatePlayersTeams(team1: Player[], team2: Player[]): void {
        this.playerService.updateTeams(team1, team2).subscribe(
            (data) => {
                console.log('updated teams successfully');
            },
            (error) => {
                console.log(error.message);
            }
        );
    }

    /**
     * @function sortPlayers() sorts players by stats
     * @param players a list of unsorted players
     * @returns a sorted list of players by stats
     */
    sortPlayers(players: Player[]): Player[] {
        return players.sort((player1, player2) => {
            if (player1.stats > player2.stats) {
                return -1; // player 1 is sorted to an index lower than player 2
            } else if (player1.stats < player2.stats) {
                return 1; // player 1 is sorted to an index higher than player 2
            }
            return 0; // player 1 or player 2 are treated as equal
        });
    }

    appendNumberOfPlayers(numOfPlayers: number): void {
        this.playingTodayString = 'Playing Today: ' + numOfPlayers;
    }

    setPlayersVote(players: Player[]): void {
        const todaysDate = this.playerService.getTodaysDate();
        for (let i = 0; i < players.length; i++) {
            if (players[i].answerdate !== todaysDate) {
                players[i].answer = 0;
            }
        }
    }

    signUp(): void {
        this.appService.navigateToComponent(DataConstants.pageNameConstantData.signupPage, false);
    }

    openLoginDialog(): void {
        const dialogData = new Map<string, any>();
        dialogData.set(DataConstants.dialogDataConstants.player, this.player);
        dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.login);
        dialogData.set(DataConstants.dialogDataConstants.displayMessage, this.loginString);
        const dialogRef = this.dialog.open(DialogComponent, {
            data: dialogData
        });

        dialogRef.afterClosed().subscribe((result) => {
            this.onDialogClose(result);
        });
    }

    /**
     * close handler for login, edit profile, update score, delete account, changes saved
     * @param result data passed in from the last open dialog
     */
    onDialogClose(result: any): void {
        if (!result) { // dialog closed without changes
            return;
        }
        this.getPlayers();
    }

    getSeventhPlayerObservable(): Observable<any> {
        return this.playerService.getSeventhPlayer()
            .flatMap(alternatePlayerQueryResult => {
                return this.deserializeAlternatePlayer(alternatePlayerQueryResult);
            });
    }

    deserializeAlternatePlayer(alternatePlayerQuery: any): Observable<Object> {
        if (alternatePlayerQuery.length === 0) {
            return of(undefined);
        }
        const { id, lastname, date } = alternatePlayerQuery[0];
        return of({ 'playerID': `${ id }`, 'lastname': `${ lastname }`, 'date': `${ date }` });
    }

    onPlayerSelect(player: Player) {
        this.player = player;
        this.playerName = player.firstname + ' ' + player.lastname;
        this.openLoginDialog();
    }
    /**
     * @function onPlayerTodayClicked opens a remove from today's players list dialog
     * @param selectedPlayer player object which got clicked on
     */
    onPlayerTodayClicked(selectedPlayer: Player): void {
        const dialogData = new Map<string, any>();
        const displayMessage = 'Delete From Today\'s Player List?';

        dialogData.set(DataConstants.dialogDataConstants.dialogName, DataConstants.dialogNames.remove);
        dialogData.set(DataConstants.dialogDataConstants.player, selectedPlayer);
        dialogData.set(DataConstants.dialogDataConstants.displayMessage, displayMessage);

        const dialogRef = this.dialog.open(DialogComponent, { data: dialogData });
        dialogRef.beforeClose().subscribe((isPlayerRemoved) => { // closehandler is a boolean value
            if (isPlayerRemoved) {
                this.getPlayers();
            }
        });
    }

    getLastScoreUpdatedTime(): Observable<string> {
        return this.playerService.getScoreUpdatedTime()
            .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
            .flatMap(lastScoreUpdateDate => {
                return this.parseScoreUpdatedDate(lastScoreUpdateDate);
            });
    }

    parseScoreUpdatedDate(scoreUpdatedDateAsJson: any): Observable<string> {
        if (scoreUpdatedDateAsJson.length > 0) {
            return of(scoreUpdatedDateAsJson[0].date);
        }
        return of('');
    }

    getTodaysDate(): string {
        return this.playerService.getTodaysDate();
    }
}
