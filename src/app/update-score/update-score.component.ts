import { Component, OnInit } from '@angular/core';
import { UpdateScoreService } from './update-score.service';
import { Player } from './../player.component/player';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DataConstants } from './../DataConstants';
import 'rxjs/add/operator/catch';
import { AppService } from './../app.component/app.component.service';
import { DialogService } from '../dialog.component/dialog.service';
@Component({
  selector: 'bball-update-score',
  templateUrl: './update-score.component.html',
  styleUrls: ['./update-score.component.css', './../css/dialog.css', './../css/font.css']
})
export class UpdateScoreComponent implements OnInit {

  private player: Player;
  public readonly dialogTitle: string = 'update score';
  public message: string;
  public isInputValid: boolean;
  public date: string;

  constructor(
    private updateScoreService: UpdateScoreService,
    private router: Router,
    private location: Location,
    private appService: AppService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.player = this.updateScoreService.getCurrentPlayer();
    if (!this.player.isAdmin) { //not admin, should not be able to visit this page.
      this.location.back();
    }
  }

  onUpdateClicked(team1Score: string, team2Score: string): void {
    if (team1Score === '' || team2Score === '' || this.date === undefined) {
      this.isInputValid = false;
      this.message = 'Scores and date must be entered!';
      return;
    }
    this.updateScoreService.updateScore(team1Score, team2Score, this.date)
      .catch((err) => this.appService.navigateToErrorPage(err.message, this.constructor.name))
      .subscribe(
        (data) => {
          alert('scores updated successfully!');
          this.appService.refreshPage();
        });
  }

  onBackClicked(): void {
    this.dialogService.navigateToNavigationComponent();
  }

  datePicked(date: string) {
    this.date = date;
  }
}
